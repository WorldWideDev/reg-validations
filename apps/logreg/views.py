# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from .models import User
from django.shortcuts import render, redirect
from django.contrib import messages

# Create your views here.
def index(request):
    return render(request, 'logreg/index.html')

def register(request):
    print request.POST
    errs = User.objects.validate_registration(request.POST)
    if errs:
        for e in errs:
            messages.error(request, e)
    else:
        # make a user
        new_user = User.objects.create_user(request.POST)
        request.session['id'] = new_user.id
        messages.success(request, "Thank you {} for registering".format(new_user.username))
    return redirect('/')